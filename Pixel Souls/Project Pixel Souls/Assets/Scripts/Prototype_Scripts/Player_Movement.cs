﻿using UnityEngine;
using System.Collections;

public class Player_Movement : MonoBehaviour {

	public float speed = 0.05f;
	public float defaultSpeed = 0.05f;
	public float runSpeed = 0.1f;
	public float rollSpeed = 2.5f;

	private Vector2 moveDirection = Vector2.zero;
	private Vector2 facingDirection = Vector2.zero;
	private Vector2 rollDirection = Vector2.zero;
	public GameObject characterImage;
	public Animator playerAnim;
	private bool isRolling = false;

	void Update() {

		moveDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
		moveDirection = transform.TransformDirection(moveDirection);
		moveDirection *= speed;

		if (Input.GetAxis("Horizontal") != 0.0f || Input.GetAxis("Vertical") != 0.0f)
		{
			playerAnim.SetBool ("isMoving", true);
		}
		else
		{
			playerAnim.SetBool ("isMoving", false);
			playerAnim.SetBool ("isRunning", false);
			speed = defaultSpeed;
		}

		if (isRolling != true) 
		{
			this.gameObject.transform.Translate (moveDirection);
		}

		if (Input.GetButtonDown ("Run"))
		{
			speed = runSpeed;
			playerAnim.SetBool ("isRunning", true);
		}

		float facingDirection = Mathf.Atan2 (Input.GetAxis ("RightStickHorizontal"), Input.GetAxis ("RightStickVertical"));

		if (Input.GetAxis("RightStickHorizontal") != 0.0f || Input.GetAxis ("RightStickVertical") != 0.0f)
		{
			characterImage.transform.rotation = Quaternion.Euler (0.0f, 0.0f, facingDirection * Mathf.Rad2Deg);
			rollDirection = new Vector2(Input.GetAxis("RightStickHorizontal"), -Input.GetAxis("RightStickVertical"));
		}

		if (Input.GetButtonDown ("Roll") && isRolling != true) 
		{
			isRolling = true;
			playerAnim.SetTrigger ("dodgeRoll");
			StartCoroutine(DodgeRoll(rollDirection));

		}

	}		

	public IEnumerator DodgeRoll(Vector2 rollDirection)
	{
		float time = 0.0f;
		rollDirection = transform.TransformDirection(rollDirection);
		rollDirection *= rollSpeed;

		while (time < 1.0f) 
		{
			time += Time.deltaTime / time;
			//this.gameObject.transform.Translate (rollDirection);
			this.gameObject.transform.position = Vector2.Lerp(transform.position, rollDirection, 5.0f);
			yield return 0;
		}

		yield return new WaitForSeconds (0.5f);

		isRolling = false;
	}

}
