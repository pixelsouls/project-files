﻿using UnityEngine;
using System.Collections;

public class Player_Turning : MonoBehaviour
{

    public float speed = 0.1f;
    public float defaultSpeed = 0.1f;
    public float runSpeed = 0.5f;
    public float rollSpeed = 1.0f;

    private Vector2 moveDirection = Vector2.zero;
    private Vector2 facingDirection;// = Vector2.zero;
    private Vector2 rollDirection = Vector2.zero;
    public GameObject characterImage;
    public Animator playerAnim;
    private bool isRolling = false;
    float setDirection;

    void Update()
    {
        /********************************MOVEMENT CONTROLS*************************************/
        moveDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed;
        /********************************MOVEMENT ANIMATION************************************/
        if (Input.GetAxis("Horizontal") != 0.0f || Input.GetAxis("Vertical") != 0.0f)
        {
            playerAnim.SetBool("isMoving", true);
        }
        else
        {
            playerAnim.SetBool("isMoving", false);
            playerAnim.SetBool("isRunning", false);
            speed = defaultSpeed;
        }
        /*******************************ROLLING ANIMATION**************************************/
        if (isRolling != true)
        {
            this.gameObject.transform.Translate(moveDirection);
        }
        /***************************RUNNING ANIMATION******************************************/
        if (Input.GetButtonDown("Run"))
        {
            speed = runSpeed;
            playerAnim.SetBool("isRunning", true);
        }
        /**************************************DIRECTION CONTROL******IT WORKZ!!!******************************************/
        //if the right stick is null. Default to using the left stuck for directional input.
        if (Input.GetAxis("RightStickHorizontal") == 0.0f && Input.GetAxis("RightStickVertical") == 0.0f)
        {
            //grab user input and store that data in the float value below. Regardless of the value (even if null).
            float facingDirection = Mathf.Atan2(Input.GetAxis("Horizontal"), -Input.GetAxis("Vertical"));
            //if the user input is greater than zero. IE: the left stick is pressed in a direction.
            if (Input.GetAxis("Horizontal") != 0.0f || Input.GetAxis("Vertical") != 0.0f)
            {
                //Sets the facing direction of the character. Uses the float value facingDirection to determine where to face.
                characterImage.transform.rotation = Quaternion.Euler(0.0f, 0.0f, facingDirection * Mathf.Rad2Deg);
            }
        }
        //if the right stick IS in use, use the right stick instead of the left.
        else
        {
            //grab user input and store that data in the float value below. Regardless of the value (even if null).
            float facingDirection = Mathf.Atan2(Input.GetAxis("RightStickHorizontal"), Input.GetAxis("RightStickVertical"));
            //if the user input is greater than zero. IE: the right stick is pressed in a direction.
            if (Input.GetAxis("RightStickHorizontal") != 0.0f || Input.GetAxis("RightStickVertical") != 0.0f)
            {
                //Sets the facing direction of the character. Uses the float value facingDirection to determine where to face.
                characterImage.transform.rotation = Quaternion.Euler(0.0f, 0.0f, facingDirection * Mathf.Rad2Deg);
            }
        }


        /****************************************WORKABLE TURNING, KINDA BROKEN**********************************************/
        /* if (Input.GetAxis("RightStickHorizontal") == 0.0f && Input.GetAxis("RightStickVertical") == 0.0f)
         {
             setDirection = Mathf.Atan2(Input.GetAxis("Horizontal"), -Input.GetAxis("Vertical"));
             characterImage.transform.rotation = Quaternion.Euler(0.0f, 0.0f, setDirection * Mathf.Rad2Deg);
         }
         else
         {
             setDirection = Mathf.Atan2(Input.GetAxis("RightStickHorizontal"), Input.GetAxis("RightStickVertical"));
             characterImage.transform.rotation = Quaternion.Euler(0.0f, 0.0f, setDirection * Mathf.Rad2Deg);
         }*/

        rollDirection = new Vector2(Input.GetAxis("RightStickHorizontal"), -Input.GetAxis("RightStickVertical"));

        if (Input.GetButtonDown("Roll") && isRolling != true)
        {
            isRolling = true;
            playerAnim.SetTrigger("dodgeRoll");
            StartCoroutine(DodgeRoll(rollDirection));

        }
    }

    public IEnumerator DodgeRoll(Vector2 rollDirection)
    {
        float time = 0.0f;
        rollDirection = transform.TransformDirection(rollDirection);
        rollDirection *= rollSpeed;

        while (time < 1.0f)
        {
            time += Time.deltaTime / time;
            this.gameObject.transform.Translate(rollDirection);
            yield return 0;
        }

        yield return new WaitForSeconds(0.5f);

        isRolling = false;
    }
}
